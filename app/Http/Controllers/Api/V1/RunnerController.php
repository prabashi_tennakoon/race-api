<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use App\Models\Runner;
use App\Http\Requests\StoreRunnerRequest;
use App\Http\Requests\UpdateRunnerRequest;
use App\Http\Resources\RunnerResource;

class RunnerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getRunner($runnerId)
    {
        $runner = Runner::with('formDatas','LastRuns')->find($runnerId);
        if ($runner == null) {
            return response()->json(["error" => 'Runner not found'], 400);
        }

        $runnerResource = new RunnerResource($runner);
        return $runnerResource->response()->setStatusCode(200);

    }

}
