<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class RunnerResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'runner_name' => $this->name,
            'age' => $this->formDatas->age,
            'colour' => $this->formDatas->colour,
            'sire' => $this->formDatas->sire,
            'dam' => $this->formDatas->dam,
            'owners' => $this->formDatas->owners,
            'last_runs' => $this->LastRuns,

          ];
    }

    public function with($request){
        return [
          'success'=> true,
          'status'=>200
        ];
    }
}
