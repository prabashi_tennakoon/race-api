<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class FormData extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'runner_id',
        'age',
        'sex',
        'colour',
        'sire',
        'dam',
        'owners'

    ];

    public function runner()
    {
        return $this->belongsTo(Runner::class);
  	}
}
