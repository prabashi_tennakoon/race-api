<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Traits\Uuids;

class Runner extends Model
{
    use HasFactory, Uuids;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'external_id',
        'race_id',
    ];

    public function race()
    {
        return $this->belongsTo(Race::class);
  	}

    public function formDatas()
    {
        return $this->hasOne(FormData::class);
    }

    public function LastRuns()
    {
        return $this->hasMany(FormLastRunner::class);
    }
}
