<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class FormLastRunner extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
    	'runner_id',
        'rate',
        'volume',
        'jockey',
        'race_date',
        'runner_status',
        'value',
        'price'

    ];

    public function runner()
    {
        return $this->belongsTo(Runner::class);
  	}
}
