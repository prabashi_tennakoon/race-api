<?php

namespace App\Traits;

use Illuminate\Support\Str;

trait Uuids
{
   /**
     * Boot function from Laravel.
     */
    protected static function boot()
    {
        parent::boot();
        static::creating(function ($model) {
            $model->{$model->getUuidColumn()} = Str::uuid()->toString();
        });
    }

    /**
     * Get the UUID column name.
     *
     * @return string
     */
    public function getUuidColumn()
    {
        return 'external_id';
    }
}
