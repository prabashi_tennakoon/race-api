<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFormLastRunnersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('form_last_runners', function (Blueprint $table) {
            $table->id();
            $table->foreignId('runner_id')->references('id')->on('runners')->onDelete('cascade');
            $table->string('rate', 20)->nullable();
            $table->string('volume', 20)->nullable();
            $table->string('jockey', 50)->nullable();
            $table->timestamp('race_date')->nullable();
            $table->string('runner_status')->nullable();
            $table->double('value')->nullable();
            $table->double('price')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('form_last_runners');
    }
}
