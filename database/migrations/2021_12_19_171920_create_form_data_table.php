<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFormDataTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('form_data', function (Blueprint $table) {
            $table->id();
            $table->foreignId('runner_id')->references('id')->on('runners')->onDelete('cascade');
            $table->integer('age')->nullable();
            $table->string('sex', 20)->nullable();
            $table->string('colour', 20)->nullable();
            $table->string('sire', 50)->nullable();
            $table->string('dam', 50)->nullable();
            $table->string('owners')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('form_data');
    }
}
