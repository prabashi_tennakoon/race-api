<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use App\Models\Race;

class RunnerFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $races = Race::all()->pluck('id');

        return [
            'race_id' => $this->faker->randomElement($races),
            'name' => $this->faker->sentence(2)
        ];
    }
}
