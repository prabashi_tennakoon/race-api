<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use App\Models\Runner;

class FormDataFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $runners = Runner::all()->pluck('id');

        return [
            'runner_id' => $this->faker->randomElement($runners),
            'age' => $this->faker->numberBetween(1,25),
            'sex' => 'Male',
            'colour' => $this->faker->sentence(1),
            'sire' => $this->faker->sentence(1),
            'dam' => $this->faker->sentence(1),
            'owners' => $this->faker->sentence(2),
        ];
    }
}
