<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use App\Models\Meeting;

class RaceFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $meetings = Meeting::all()->pluck('id');

        return [
            'meeting_id' => $this->faker->randomElement($meetings),
            'name' => $this->faker->sentence(2)
        ];
    }
}
