<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use App\Models\Runner;

class FormLastRunnerFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $runners = Runner::all()->pluck('id');

        return [
            'runner_id' => $this->faker->randomElement($runners),
            'rate' => '3/10',
            'volume' => '2.6L',
            'jockey' => $this->faker->sentence(1),
            'race_date' => $this->faker->date(),
            'runner_status' => $this->faker->sentence(2),
            'value' => $this->faker->numberBetween(0,100000),
            'price' => $this->faker->randomNumber(2)
        ];
    }
}
