<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        \App\Models\Meeting::factory(5)->create();
        \App\Models\Race::factory(5)->create();
        \App\Models\Runner::factory(5)->create();
        \App\Models\FormData::factory(5)->create();
        \App\Models\FormLastRunner::factory(5)->create();

    }
}
