*********** Steps to run the Laravel API in Local(Race API) *************

run $composer update
run $php artisan config:cache
start MySQL
create Database - 'gtl_tech'
run $php artisan migrate
run $ php artisan db:seed
run $php artisan serve (http://127.0.0.1:8000)

import postman collection - https://www.getpostman.com/collections/70dc52a38ffaed96f754

* Composer should be installed.